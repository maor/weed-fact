#!/usr/bin/python3
from email.message import EmailMessage
from urllib.request import Request, urlopen
from pathlib import Path
from random import choice
import json
import logging
import sys

API_KEY = Path("~/.b2-api-key-base64").expanduser().read_text().strip()


def get_token_and_api_url() -> tuple[str, str]:
    req = Request("https://api.backblazeb2.com/b2api/v3/b2_authorize_account")
    req.add_header("Authorization", f"Basic {API_KEY}")
    with urlopen(req) as resp_file:
        resp = json.load(resp_file)
        return resp["authorizationToken"], resp["apiInfo"]["storageApi"]["apiUrl"]


def list_files_in_bucket(token: str, api_url: str) -> list[str]:
    """Returns file IDs to used for constructing Native URLs"""
    req = Request(f"{api_url}/b2api/v3/b2_list_file_names?bucketId=dce8e32f2ecdae3981dd0b19")
    req.add_header("Authorization", token)
    with urlopen(req) as resp_file:
        return [f["fileId"] for f in json.load(resp_file)["files"]]


def random_pic() -> str:
    token, api_url = get_token_and_api_url()
    file_id = choice(list_files_in_bucket(token, api_url))
    return f"{api_url}/b2api/v1/b2_download_file_by_id?fileId={file_id}"


def make_message(pic_url: str) -> EmailMessage:
    print("Picture picked: %s", pic_url, file=sys.stderr)
    msg = EmailMessage()
    msg["Subject"] = "עובדה יומית"
    msg["From"] = "Gadi Wilcherski <wilcherski@avocadosh.xyz>"
    msg["To"] = "Epic Gang <epic-gang@stoner-bros.dope.il>"
    msg.set_content(f'<img src="{pic_url}" />', "html")
    return msg


def main():
    print(make_message(random_pic()))


if __name__ == "__main__":
    main()
